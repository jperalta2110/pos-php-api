# POS PHP Api

--------------------
* Author: Jorge Peralta
* Email: jperalta0789@gmail.com

## Overview

--------------------
A point-of-sale API that is executable as a shell script.

## Task

--------------------
Implement a point-of-sale scanning API that accepts an arbitrary ordering of products (similar to what would happen at a checkout line) and then returns the correct total price for an entire shopping cart based on the per unit prices or the volume prices as applicable.

Here are the products listed by code and the prices to use (there is no sales tax):
Product Code | Price
--------------------
A | $2.00 each or 4 for $7.00

B | $12.00

C | $1.25 or $6 for a six pack

D | $0.15

--------------------

There should be a top level point of sale terminal service object that looks something like the pseudo-code below. You are free to design and implement the rest of the code however you wish, including how you specify the prices in the system:

terminal->setPricing(...)

terminal->scan("A")

terminal->scan("C")

... etc.

result = terminal->total

--------------------

Here are the minimal inputs you should use for your test cases. These test cases must be shown to work in your program:

Scan these items in this order: ABCDABAA; Verify the total price is $32.40.

Scan these items in this order: CCCCCCC; Verify the total price is $7.25.

Scan these items in this order: ABCD; Verify the total price is $15.40.

## Usage

--------------------

######Please note that Pos.php is the entry point for this API.

--------------------

####Set product pricing:

```
php Pos.php --setPricing --name A --price 2 --volume 4 --volume_price 7
```
* __setPricing__: Method saved product data, all parameters are required.
* __name__: Product name.
* __price__: Product price,
* __volume__: Quantity required to be volume price eligible.
* __volume_price__: Volume price.

--------------------

####Scan an item:
```
php Pos.php --scan --name A
```
* __scan__: Method scans an item into shopping cart.
* __name__: Product name.

--------------------

####Scan multiple items:

######Built for debugging purposes.

```
php Pos.php --multiScan ABCDABCD
```
* __multiScan__: Method takes on a string of single character "product names" that are then scanned in bulk.

--------------------

####Get Cart Total:

```
php Pos.php --total
```
* __total__: Method displays cart total and empties cart.

## Installation

Available options:

* Directly from github - download the latest release from the release section
