<?php
/**
 * @author      Jorge Peralta (jperalta0789@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

define('DS', DIRECTORY_SEPARATOR);
define('BP', dirname(__FILE__));

require (BP . DS . 'Lib' . DS . 'Autoload.php');

class Pos extends Lib_Shell
{
    /**
     * Run script
     *
     */
    public function run()
    {
        if($this->getArg('setPricing')){
            $product = new Product();
            $product->setDataFromArray($this->getArgArray());
        }
        elseif($this->getArg('scan')){
            try{
                $cart = new Cart();
                $cart->scanItem($this->getArg('name'));
            } catch (Exception $e){
                die($e);
            }
        }
        elseif($this->getArg('multiScan')){
            try{
                $items = str_split($this->getArg('multiScan'));
                $cart = new Cart();
                foreach ($items as $item){
                    $cart->scanItem($item);
                }
            } catch (Exception $e){
                die($e);
            }
        }
        elseif($this->getArg('total')){
            try{
                $cart = new Cart();
                $cart->getTotal();
            } catch (Exception $e){
                die($e);
            };
        }
        else {
            echo $this->usageHelp();
        }
    }

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php -f pos.php -- [options]
  setPricing    Set pricing for an item.
    requires:
        name            Name of product.
        price           Price of product.
        volume          Quantity to be volume price eligible.
        volume_price    Volume price.
        
  scan          Scan an item into cart.
    requires:
        name            Name of product.
        
  multiScan     Scan multiple items into cart. All items are a single character.
        
  total         Get cart total.

USAGE;
    }
}

$shell = new Pos();
$shell->run();
