<?php

/**
 * @author      Jorge Peralta (jperalta0789@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
class Cart
{
    /**
     * @var array
     */
    private $_cart = array();

    /**
     * @var Cart_Model
     */
    private $_cartModel;

    /**
     * @var array
     */
    private $_productCache = array();

    /**
     * @var string
     */
    private $_cartTotal;

    /**
     * Cart constructor.
     */
    public function __construct()
    {
        $this->_cartModel = new Cart_Model();
        $this->getExistingCart();
    }

    /**
     * Get existing cart if it exists.
     */
    private function getExistingCart()
    {
        $cartItems = $this->_cartModel->getCartItemsFromFileIfExist();

        if ($cartItems) {
            $this->_cart = $cartItems;
        }
    }

    /**
     * Scan an item into the cart.
     *
     * @param $productName
     * @throws Exception
     */
    public function scanItem($productName)
    {
        if (!$productName) {
            throw new Exception('Product name was not set.');
        }

        $this->_verifyCanAddItemToCart($productName);
        $this->_addItemToCart($productName);
        $this->_printProductAdded($productName);
        $this->_saveCart();
    }

    /**
     * @param $name
     * @throws Exception
     */
    private function _verifyCanAddItemToCart($name)
    {
        $product = new Product();
        $available = $product->isProductAvailable($name);

        if (!$available) {
            throw new Exception('Product does not exist.');
        }

        return;
    }

    /**
     * Add and item to the cart, increase item quantity if already in cart.
     *
     * @param $productName
     */
    private function _addItemToCart($productName)
    {
        try {
            $this->_verifyCanAddItemToCart($productName);

            if (!array_key_exists($productName, $this->_cart)) {
                $this->_cart[$productName] = 0;
            }
            $this->_cart[$productName] += 1;
        } catch (Exception $e)
        {
            die($e);
        }
    }

    /**
     * Attempt to save cart.
     */
    private function _saveCart()
    {
        try {
            $this->_cartModel->setData($this->_cart);
            $this->_cartModel->saveCart();
        } catch (Exception $e) {
            die($e);
        }
    }

    /**
     * @throws Exception
     */
    public function getTotal()
    {
        if(empty($this->_cart)){
            throw new Exception('The cart is empty. Please scan an item.');
        }

        $this->_addProductDataToCache();
        $this->_collectTotals();
        $this->_printTotal();
        $this->_cartModel->emptyCart();
    }

    /**
     * We save product data to $_productCache.
     */
    private function _addProductDataToCache(){

        // name is not a product so we remove it.
        unset($this->_cart['name']);

        $productModel = new Product_Model();

        foreach ($this->_cart as $itemName => $qty){
            $this->_productCache[$itemName] = $productModel->getDataFromFileByName($itemName);
        }
    }

    /**
     * Collect totals from volume and regular prices.
     */
    private function _collectTotals(){
        $total = 0;

        foreach ($this->_cart as $itemName => $qty){
            $currentProduct = $this->_productCache[$itemName];

            if($currentProduct['volume'] > 0){
                $volumePrice = floor($qty / $currentProduct['volume']);
                $qty = $qty - ($volumePrice * $currentProduct['volume']);

                $total += $volumePrice * $currentProduct['volumePrice'];
            }

            $total += $qty * $currentProduct['price'];
        }

        $this->_cartTotal = $this->_formatTotal($total);
    }

    /**
     * Print total to console.
     */
    private function _printTotal(){
        echo "Your total is: " . $this->_cartTotal . "\n";
    }

    /**
     * Print total to console.
     *
     * @param $productName string
     */
    private function _printProductAdded($productName){
        echo "Item " . $productName . " has been added to your cart.\n";
    }

    /**
     * Format total as USD.
     *
     * @param $total
     * @return string
     */
    private function _formatTotal($total){
        return '$' . number_format($total, 2);
    }
}