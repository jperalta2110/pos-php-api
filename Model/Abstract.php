<?php

/**@author      Jorge Peralta (jperalta0789@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
abstract class Model_Abstract
{
    const FILE_PATH = 'Model/Data';
    const FILE_TYPE = '.txt';

    /**
     * @var stdClass
     */
    protected $_data;

    /**
     * @var array
     */
    protected $_requiredKeys = array();


    /**
     * Product_Data constructor.
     */
    public function __construct()
    {
        $this->_data = new stdClass();
    }

    /**
     * @return stdClass
     */
    public function getData()
    {
        return $this->_data;
    }

    /**
     * @param $data array|stdClass
     * @return bool
     * @throws Exception
     */
    public function setData($data)
    {
        if (gettype($data) == "array") {
            $this->_saveArrayToData($data);
            return true;
        }

        if (get_class($data) == "stdClass") {
            $this->_data = $data;
            return true;
        }

        throw new Exception('Unable to save data');
    }

    /**
     * Attempt to convert array to stdClass data.
     *
     * @param $dataArray
     */
    private function _saveArrayToData($dataArray)
    {
        $this->_data = json_decode(json_encode($dataArray));
    }

    /**
     * Attempt to convert array to stdClass data.
     *
     * @param $data
     * @return array
     */
    public function convertDataToArray($data)
    {
        return json_decode(json_encode($data), true);
    }

    /**
     * Ensure that $dataArray contains all $_requiredKeys.
     *
     * @param $dataArray
     * @throws Exception
     */
    protected function _validateDataArray($dataArray)
    {
        foreach ($this->_requiredKeys as $requiredKey) {
            if (!array_key_exists($requiredKey, $dataArray)) {
                throw new Exception('Required data not provided: ' . $requiredKey);
            }
        }
    }

    /**
     * Save $_data to file.
     */
    public function saveDataToFile()
    {
        $filePath = $this->_getFilePath();

        $objData = serialize($this->_data);

        $fp = fopen($filePath, "w");
        fwrite($fp, $objData);
        fclose($fp);
    }

    /**
     * Save $_data to file.
     *
     * @param $name
     */
    private function _setDataFromFileByName($name)
    {
        $this->_data->name = $name;
        $filePath = $this->_getFilePath();
        $fp = fopen($filePath, "rb");
        $this->_data = fgets($fp);
        $this->_data = unserialize($this->_data);
        fclose($fp);
    }

    /**
     * Set product data from file and return data.
     *
     * @param $name
     * @return array|bool
     */
    public function getDataFromFileByName($name)
    {
        if (!$this->doesFileExist($name)) {
            return false;
        }

        $this->_setDataFromFileByName($name);

        $dataArray = $this->convertDataToArray($this->_data);

        return $dataArray;
    }

    /**
     * Concatinates file path using constants and file name.
     *
     * @param $filePath
     * @param $fileType
     * @return string
     */
    protected function _getFilePath($filePath = self::FILE_PATH, $fileType = self::FILE_TYPE)
    {
        $filePath = str_replace('/', DS, $filePath);

        return BP . DS . $filePath . DS . $this->_data->name . $fileType;
    }

    /**
     * Checks if a file exists.
     *
     * @param $name string
     * @return bool
     */
    public function doesFileExist($name = null)
    {
        if (!is_null($name)) {
            $this->_data->name = $name;
        }

        $filePath = $this->_getFilePath();

        return file_exists($filePath);
    }

    /**
     * Delete file.
     */
    protected function _deleteFile()
    {
        $filePath = $this->_getFilePath();
        unlink($filePath);
    }

}