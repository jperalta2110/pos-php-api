<?php
/**
 * @author      Jorge Peralta (jperalta0789@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class Product_Model extends Model_Abstract
{
    const FILE_PATH = 'Product/Data';
    const FILE_TYPE = '.txt';

    protected $_requiredKeys = array(
        'name',
        'price',
        'volume',
        'volume_price'
    );

    /**
     * Product_Data constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $data array|stdClass
     * @return bool
     * @throws Exception
     */
    public function setData($data){
        if(gettype($data) == "array"){
            $this->_saveArrayToData($data);
            return true;
        }

        return parent::setData($data);
    }

    /**
     * Attempt to convert array to stdClass data.
     * @param $dataArray
     */
    private function _saveArrayToData($dataArray){
        try{
            $this->_validateDataArray($dataArray);

            $this->_data->name = $dataArray['name'];
            $this->_data->price = $dataArray['price'];
            $this->_data->volume = $dataArray['volume'];
            $this->_data->volumePrice = $dataArray['volume_price'];

        } catch (Exception $e){
            die($e);
        }
    }

    /**
     * Concatinates file path using constants and file name.
     *
     * @param string $filePath
     * @param string $fileType
     * @return string
     */
    protected function _getFilePath($filePath = self::FILE_PATH, $fileType = self::FILE_TYPE){
        return parent::_getFilePath($filePath, $fileType);
    }
}