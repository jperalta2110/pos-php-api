<?php

/**
 * @author      Jorge Peralta (jperalta0789@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class Product
{
    /**
     * @var stdClass
     */
    private $_productData;

    /**
     * @var Product_Model
     */
    private $_productModel;

    /**
     * Product constructor.
     */
    public function __construct()
    {
        $this->_productModel = new Product_Model();
    }

    /**
     * Set data from an array.
     *
     * @param $array
     */
    public function setDataFromArray($array){
        try{
            $this->_productModel->setData($array);
            $this->_productModel->saveDataToFile();
            $this->_productData = $this->_productModel->getData();
        }catch (Exception $e){
            die($e);
        }
    }

    /**
     * Get product by name.
     *
     * @param $name
     * @return $this
     */
    public function getProductByName($name){
        $this->_productData = $this->_productModel->getDataFromFileByName($name);
        return $this;
    }

    /**
     * Check if product exists.
     *
     * @param $name
     * @return bool
     */
    public function isProductAvailable($name){
        return $this->_productModel->doesFileExist($name);
    }

}