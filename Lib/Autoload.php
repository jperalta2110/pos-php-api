<?php
/**
 * @author      Jorge Peralta (jperalta0789@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

/**
 * Autoload function that uses Magento naming convention.
 */
spl_autoload_register(function ($className){
    $file = str_replace('_', DS, $className) . ".php";
    require BP . DS . $file;
});