<?php
/**
 * @author      Jorge Peralta (jperalta0789@gmail.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class Cart_Model extends Model_Abstract
{
    const FILE_PATH = 'Cart/Data';
    const FILE_NAME = 'cart_items';
    const FILE_TYPE = '.txt';

    /**
     * Product_Data constructor.
     * @param $dataArray array
     */
    public function __construct($dataArray = null)
    {
        parent::__construct();
    }

    /**
     * Get cart from file if exists.
     *
     * @return stdClass|bool
     */
    public function getCartItemsFromFileIfExist(){
        $cart = parent::getDataFromFileByName(self::FILE_NAME);
        return $cart;
    }

    /**
     * Concatinates file path using constants and file name.
     *
     * @param string $filePath
     * @param string $fileType
     * @return string
     */
    protected function _getFilePath($filePath = self::FILE_PATH, $fileType = self::FILE_TYPE){
        return parent::_getFilePath($filePath, $fileType);
    }

    /**
     * Set file name and save cart to file.
     */
    public function saveCart(){
        $this->_data->name = self::FILE_NAME;
        $this->saveDataToFile();
    }

    /**
     * Empty cart by deleting file.
     */
    public function emptyCart(){
        $this->_deleteFile();
    }
}